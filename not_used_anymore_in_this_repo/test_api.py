"""
Integrated software project - Electrical energy at a glance - Team 2

Unit tests of the data_integration module

Author: Dominik Zians
"""

# Own modules
import data_client.openapi_client as data_client
import auth_client.openapi_client as auth_client
from data_client.openapi_client.rest import ApiException as DataApiException
from auth_client.openapi_client.rest import ApiException as AuthApiException

#from main import Building, init_buildings, config_access_token

# Standart library
import os
from datetime import datetime

# Other
import pytest
import pandas as pd


# TODO: all this looks like it is the unit test of the api python client
#       need to add the building class and the other functions of the main programm
#       => refactored the main code for this purpose, therefore this file is not used anymore


@pytest.fixture
def auth_configuration():
    return auth_client.Configuration(
        host=f"http://{os.environ['AUTH_API_HOST']}"
             f":{os.environ['AUTH_API_PORT']}/1.0.0")


@pytest.fixture
def data_config_without_token():
    return data_client.Configuration(
        host=f"http://{os.environ['DATA_API_HOST']}"
             f":{os.environ['DATA_API_PORT']}/1.0.0")


@pytest.fixture
def data_config_with_token(auth_configuration):
    with auth_client.ApiClient(auth_configuration) as api_client:
        # Create an instance of the API class
        auth_api_instance = auth_client.DefaultApi(api_client)

        # Create info for login
        authentication_info = auth_client.AuthenticationInfo(
            os.environ['API_USER'],
            os.environ['API_PASSWD'],
            response_type='token')

        # Get a list of buildings
        login = auth_api_instance.login(authentication_info)
        api_token = login[2]["Location"].split('#')[1].split('&')[0].split('=')[1]

    return data_client.Configuration(
        host=f"http://{os.environ['DATA_API_HOST']}"
             f":{os.environ['DATA_API_PORT']}/1.0.0",
        access_token=api_token)


@pytest.fixture
def create_test_buildings(data_config_with_token):
    # TODO: add buildings
    return 'B42'


def test_get_buildings(data_config_without_token):
    with data_client.ApiClient(data_config_without_token) as api_client:
        # Create an instance of the API class
        api_instance = data_client.DefaultApi(api_client)

        # Get a list of buildings
        buildings = api_instance.get_buildings()

    # TODO: test the output
    # TODO: add fixture that creates buildings before


def test_get_building_info(data_config_without_token):
    with data_client.ApiClient(data_config_without_token) as api_client:
        # Create an instance of the API class
        api_instance = data_client.DefaultApi(api_client)

        # Get a list of buildings
        info = api_instance.get_building_info('building')

    # TODO: test the output
    # TODO: add fixture that creates the building before


def test_get_data(data_config_without_token):
    with data_client.ApiClient(data_config_without_token) as api_client:
        # Create an instance of the API class
        api_instance = data_client.DefaultApi(api_client)

        # Get a list of buildings
        data = api_instance.get_data(
            'building',
            'datetime',
            'datetime')

    # TODO: test the output
    # TODO: add fixture that add building & data


def test_post_data_produnction(data_config_with_token, create_test_building_production):
    with data_client.ApiClient(data_config_with_token) as api_client:
        # Create an instance of the API class
        data_api_instance = data_client.DefaultApi(api_client)

        # Post a list of data points
        data_api_instance.post_data(
            create_test_building_production,
            pd.DataFrame(
                data={'power': [1, 2, 3, 4]},
                index=pd.Index(
                    [
                        datetime(2020, 1, 2, 16, 20, 12),
                        datetime(2020, 1, 2, 16, 21, 31),
                        datetime(2020, 1, 2, 16, 22, 55),
                        datetime(2020, 1, 2, 16, 23, 1)],
                    name='datetime')).to_json(
                        orient='split', date_format='iso'))

        # HERE IS AN "HTTP response body: "Request body is malformed"" error
        # I suspect it is because the building does not exist in the test database
        # TODO: make a fixture that adds a building before


def test_post_data_consumption(data_config_with_token, create_test_building_consumption):
    pass  # TODO
