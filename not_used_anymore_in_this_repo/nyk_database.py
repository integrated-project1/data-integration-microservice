"""
Integrated software project - Electrical energy at a glance - Team 2

NYK-database, this file will be replaced by the data-api

Author: Dominik Zians
"""


import os
import sqlalchemy as sqla
import pandas as pd
from datetime import datetime

from database_split import Consumption, Production
from database_split import engine as nyk_engine


def store_nyk_consumption(dataframe):
    """Store a dataframe into the NYK-database

    Parameters
    ----------
    dataframe : pd.DataFrame
    """
    dataframe.to_sql('consumption', nyk_engine, if_exists='append')


def store_nyk_production(dataframe):
    """Store a dataframe into the NYK-database

    Parameters
    ----------
    dataframe : pd.DataFrame
    """
    dataframe.to_sql('production', nyk_engine, if_exists='append')


def get_nyk_consumption(session, building, start_date, end_date):
    """Get consumption data from the NYK-database

    Parameters
    ----------
    session : sqla.Session
    building : str
    start_date : datetime
    end_date : datetime

    Returns
    -------
    pd.DataFrame
    """
    query = session.query(Consumption)\
        .filter(sqla.and_(
            Consumption.datetime.between(start_date, end_date),
            Consumption.building == building))\
        .order_by(Consumption.datetime)
    return pd.read_sql(query.statement, nyk_engine, index_col='datetime')


def get_nyk_production(session, building, start_date, end_date):
    """Get production data from the NYK-database

    Parameters
    ----------
    session : sqla.Session
    building : str
    start_date : datetime
    end_date : datetime

    Returns
    -------
    pd.DataFrame
    """
    query = session.query(Production)\
        .filter(sqla.and_(
            Production.datetime.between(start_date, end_date),
            Production.building == building))\
        .order_by(Production.datetime)
    return pd.read_sql(query.statement, nyk_engine, index_col='datetime')



###############################################################################
# Example                                                                     #
###############################################################################

if __name__ == '__main__':
    # Example usecase: copy data from the old database
    import data_sources as ds

    from_date = datetime(2019, 1, 1, 0, 0)
    to_date = datetime(2019, 12, 31, 23, 59)

    for building in ['b28', 'b52']:  # Consumption
        # raw data extraction
        df = ds.get_raw_old_consumption(building, from_date, to_date)
        # Add building column
        df['building'] = building
        # Save dataframe into nyk database
        store_nyk_consumption(df)

    for building in [f'parking{i+1}' for i in range(8)]:  # Production
        # raw data extraction
        df = ds.get_raw_old_production(building, from_date, to_date)
        # Add building column
        df['building'] = building
        # Save dataframe into nyk database
        store_nyk_production(df)

    # how to get a session for get_nyk_consumption and get_nyk_production
    #Session = sqla.orm.sessionmaker(bind=nyk_engine)
    #session = Session()
    #get_nyk_consumption(session, ...)
    #get_nyk_production(session, ...)
