"""
Integrated software project - Electrical energy at a glance - Team 2

Test database inheritance models

Author: Dominik Zians
"""

from database_split import engine as engine_split
from database_single import engine as engine_single

from database_split import Consumption, Production
from database_single import MeasurementConsumption, MeasurementProduction

import data_sources as ds
import data_integration as di

from datetime import datetime
from time import time

from sqlalchemy.orm import Query
import sqlalchemy as sqla
import pandas as pd

SessionSplit = sqla.orm.sessionmaker(bind=engine_split)
SessionSingle = sqla.orm.sessionmaker(bind=engine_single)

if __name__ == '__main__':
    """times_split = []
    times_single = []

    for building, handler in ds.handlers.items():
        print(f'start {building}')
        # get one year of data
        df = handler(
            datetime(2019, 8, 1, 0, 0),
            datetime(2020, 7, 31, 23, 59))
            #datetime(2019, 8, 1, 23, 59))

        # preprocess data
        if building in {'B28', 'B52'}:
            df_prep = di.preprocess_consumption(
                df, None, None,
                min_val = {  # very loose minima
                    'voltage_12': 14500,
                    'voltage_23': 14500,
                    'voltage_31': 14500,
                    'current_1': 0,
                    'current_2': 0,
                    'current_3': 0,
                    'active_power': 0,
                    'reactive_power': 0,
                    'cos_phi': 0.5},
                max_val = {  # very loose maxima
                    'voltage_12': 16500,
                    'voltage_23': 16500,
                    'voltage_31': 16500,
                    'current_1': 100,
                    'current_2': 100,
                    'current_3': 100,
                    'active_power': 1000,
                    'reactive_power': 1000,
                    'cos_phi': 1.0})
        else:
            df_prep = di.preprocess_production(
                df, None, None,
                {'power': 0},  # very loose minima
                {'power': 150},  # very loose maxima
                5.557556, 50.585354)

        # Add building column
        df_prep['building'] = building

        print(f'db push {building}')

        # Add data to the concrete table inheritance db
        start_time = time()
        if building in {'B28', 'B52'}:
            df_prep.to_sql('consumption', engine_split, if_exists='append')
        else:
            df_prep.to_sql('production', engine_split, if_exists='append')
        times_split.append(time()-start_time)

        # Add data to the single table inheritance db
        if building in {'B28', 'B52'}:
            df_prep['type'] = 'cons'
        else:
            df_prep['type'] = 'prod'

        start_time = time()
        df_prep.to_sql('measurements', engine_single, if_exists='append')
        times_single.append(time()-start_time)

    print(f'INSERTION:\nsingle:{sum(times_single)}\nsplit:{sum(times_split)}')"""


    times_split = []
    times_single = []

    session_split = SessionSplit()
    session_single = SessionSingle()

    for building in ds.handlers:
        print(building)
        if building in {'B28', 'B52'}:
            query_split = session_split.query(
                Consumption.datetime,
                Consumption.voltage_12,
                Consumption.voltage_23,
                Consumption.voltage_31,
                Consumption.current_1,
                Consumption.current_2,
                Consumption.current_3,
                Consumption.active_power,
                Consumption.reactive_power,
                Consumption.cos_phi,
            ).filter(sqla.and_(
                Consumption.datetime.between(
                    datetime(2019, 8, 1, 0, 0),
                    datetime(2020, 7, 31, 23, 59)),
                Consumption.building == building)
            ).order_by(Consumption.datetime)

            query_single = session_single.query(
                MeasurementConsumption.datetime,
                MeasurementConsumption.voltage_12,
                MeasurementConsumption.voltage_23,
                MeasurementConsumption.voltage_31,
                MeasurementConsumption.current_1,
                MeasurementConsumption.current_2,
                MeasurementConsumption.current_3,
                MeasurementConsumption.active_power,
                MeasurementConsumption.reactive_power,
                MeasurementConsumption.cos_phi,
            ).filter(sqla.and_(
                MeasurementConsumption.datetime.between(
                    datetime(2019, 8, 1, 0, 0),
                    datetime(2020, 7, 31, 23, 59)),
                MeasurementConsumption.building == building)
            ).order_by(MeasurementConsumption.datetime)
        else:
            query_split = session_split.query(
                Production.datetime,
                Production.power
            ).filter(sqla.and_(
                Production.datetime.between(
                    datetime(2019, 8, 1, 0, 0),
                    datetime(2020, 7, 31, 23, 59)),
                Production.building == building)
            ).order_by(Production.datetime)

            query_single = session_single.query(
                MeasurementProduction.datetime,
                MeasurementProduction.power
            ).filter(sqla.and_(
                MeasurementProduction.datetime.between(
                    datetime(2019, 8, 1, 0, 0),
                    datetime(2020, 7, 31, 23, 59)),
                MeasurementProduction.building == building)
            ).order_by(MeasurementProduction.datetime)

        # Retrieve the data from the concrete table inheritance db
        start_time = time()
        df = pd.read_sql(
            query_split.statement, engine_split, index_col='datetime')
        times_split.append(time()-start_time)

        # Retrieve the data from the single table inheritance db
        start_time = time()
        df = pd.read_sql(
            query_single.statement, engine_single, index_col='datetime')
        times_single.append(time()-start_time)

    print(f'RETRIEVAL:\nsingle:{sum(times_single)}\nsplit:{sum(times_split)}')
