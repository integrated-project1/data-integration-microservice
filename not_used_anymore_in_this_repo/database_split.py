"""
Integrated software project - Electrical energy at a glance - Team 2

Database with "Concrete Table Inheritance"
see https://docs.sqlalchemy.org/en/13/orm/inheritance.html#concrete-table-inheritance

Author: Dominik Zians
"""


import os
import sqlalchemy as sqla
from sqlalchemy.ext.declarative import declarative_base


## For local tests
#engine = sqla.create_engine('sqlite:///:memory:')
engine = sqla.create_engine("mysql://root:rootroot@localhost/nyk_split")

## For server
#engine = sqla.create_engine(
#    f"mysql://{os.environ['nyk_user']}:{os.environ['nyk_pwd']}"
#    f"@{os.environ['nyk_host']}/{os.environ['nyk_database']}")

Base = declarative_base()


class Consumption(Base):
    __tablename__ = 'consumption'

    building = sqla.Column(sqla.String(10), primary_key=True, nullable=False)
    datetime = sqla.Column(sqla.DateTime, primary_key=True, nullable=False)
    voltage_12 = sqla.Column(sqla.Float)
    voltage_23 = sqla.Column(sqla.Float)
    voltage_31 = sqla.Column(sqla.Float)
    current_1 = sqla.Column(sqla.Float)
    current_2 = sqla.Column(sqla.Float)
    current_3 = sqla.Column(sqla.Float)
    active_power = sqla.Column(sqla.Float)
    reactive_power = sqla.Column(sqla.Float)
    cos_phi = sqla.Column(sqla.Float)


class Production(Base):
    __tablename__ = 'production'

    building = sqla.Column(sqla.String(10), primary_key=True, nullable=False)
    datetime = sqla.Column(sqla.DateTime, primary_key=True, nullable=False)
    power = sqla.Column(sqla.Float)


Base.metadata.create_all(engine)

if __name__ == '__main__':  # this code is not executed when importing this file
    # Example usecase:
    # Create one day of artificial data and store it into the database
    import random
    from datetime import datetime
    from time import time
    import pandas as pd
    from sqlalchemy.orm import sessionmaker
    Session = sessionmaker(bind=engine)
    session = Session()

    cons = [
        Consumption(
            building=f'B{b}',
            datetime=datetime(2020, 4, 20, i//60, i%60),
            voltage_12=random.random(),
            voltage_23=random.random(),
            voltage_31=random.random(),
            current_1=random.random(),
            current_2=random.random(),
            current_3=random.random(),
            active_power=random.random(),
            reactive_power=random.random(),
            cos_phi=random.random()
        )
        for i in range(60*24) for b in range(3)]  # 3 Buildings 24 hours

    prod = [
        Production(
            building=f'P{b}',
            datetime=datetime(2020, 4, 20, i//60, i%60),
            power=random.random()
        )
        for i in range(60*24) for b in range(8)]  # 8 PV stations 24 hours

    start_time = time()
    session.add_all(cons)
    session.add_all(prod)
    session.commit()
    print(f"Finished in {time()-start_time} seconds!")
