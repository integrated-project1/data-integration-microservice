"""
Integrated software project - Electrical energy at a glance - Team 2

Functions for data-preprocessing and data-integration with pandas

Author: Dominik Zians
"""

# Standard library
from datetime import time

# Others
import pandas as pd
import numpy as np
import pytz


###############################################################################
# Constants                                                                   #
###############################################################################


SUNRISE_SECURITY_MARGE = 3  # in minutes
SUNSET_SECURITY_MARGE = 3  # in minutes


###############################################################################
# Utils                                                                       #
###############################################################################


def compute_sunrise_sunset(from_date, to_date, longitude, latitude):
    """Compute sunrise and sunset in UTC for a given location

    see https://en.wikipedia.org/wiki/Sunrise_equation for details

    Parameters
    ----------
    from_date : datetime or string
        The first date for which to compute sunrise and sunset
        This should be only a date (no time)
    to_date : datetime or string
        The last date for which to compute sunrise and sunset (included)
        This should be only a date (no time)
    longitude : float
        The longitude of the location for which to compute sunrise and sunset
    latitude : float
        The latitude of the location for which to compute sunrise and sunset

    Returns
    -------
    tuple(numpy.array(dtype=datetime64), numpy.array(dtype=datetime64))
        The sunrise datetimes and sunset datetimes in their respective arrays,
    """
    # Compute the list of dates between from_date and to_date ...
    dr = np.arange(
        from_date,
        # arange creates values within the half-open interval, but we want to
        # include to_date, therefore add a day
        np.datetime64(to_date) + np.timedelta64(1, 'D'),
        dtype='datetime64[D]')

    # ... expressed in days since 2000-1-1
    n = (dr - np.datetime64('2000-01-01')).astype('timedelta64[D]').astype(int)

    # Compute the time of sun apogee
    J_star = n - longitude/360
    M_deg = (357.5291 + 0.98560028 * J_star) % 360
    M = np.radians(M_deg)
    C = 1.9148*np.sin(M) + 0.02*np.sin(2*M) + 0.003*np.sin(3*M)
    lam = np.radians((M_deg + C + 180 + 102.9372) % 360)
    J_transit = J_star + 0.0053*np.sin(M) - 0.0069*np.sin(2*lam)

    # Compute how long it takes between sunrise and apogee
    delta = np.arcsin(np.sin(lam) * np.sin(np.radians(23.44)))
    phi = np.radians(latitude)
    n = np.sin(np.radians(-0.83)) - np.sin(phi) * np.sin(delta)
    d = np.cos(phi) * np.cos(delta)
    omega_0 = np.degrees(np.arccos(n/d)) / 360

    # Compute datetime objects
    noon2000 = np.datetime64('2000-01-01T12:00')
    sunrise = ((J_transit - omega_0)*24*60).astype('timedelta64[m]') + noon2000
    sunset = ((J_transit + omega_0)*24*60).astype('timedelta64[m]') + noon2000

    return sunrise, sunset


###############################################################################
# Data processing functions                                                   #
###############################################################################


def round_minutes(dataframe, freq='min'):
    """Round the values of the datetime column so that seconds are 0

    It is possible that there are some duplicate rows in the output dataframe

    Parameters
    ----------
    dataframe : pd.DataFrame
        Dataframe containing a timeseries (index == pd.DatetimeIndex)
    freq : str
        Frequency of the new index, default = '1T' == one minute
        see for possible values:
        https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases

    Returns
    -------
    pd.DataFrame
        Contains the cleaned data
    """
    if not isinstance(dataframe.index, pd.DatetimeIndex):
        # only DatetimeIndex has the round() method
        raise ValueError('dataframe.index is not a pandas.DatetimeIndex!')
    dataframe.index = dataframe.index.round(freq)
    return dataframe


def remove_duplicate_rows(dataframe):
    """Remove duplicate rows from a dataframe

    Parameters
    ----------
    dataframe : pd.DataFrame
        Dataframe containing a timeseries (index == pd.DatetimeIndex)

    Returns
    -------
    pd.DataFrame
        Contains the cleaned data
    """
    return dataframe.groupby(dataframe.index).mean()


def add_missing_rows(dataframe, start_datetime=None, end_datetime=None, freq='1min'):
    """Add missing rows filled with n/a so that there is a datapoint for every minute

    Parameters
    ----------
    dataframe : pd.DataFrame
        Dataframe containing a timeseries (index == pd.DatetimeIndex)
    start_datetime : datetime or None
        Start date of the data (needed to detect missing rows at the beginning)
        It needs to be rounded to the given freqency!
        If None, missing rows at the beginning are not detected
    end_datetime : datetime or None
        End date of the data (needed to detect missing rows at the end)
        If None, missing rows at the end are not detected
    freq : str
        Frequency of the new index, default = '1min' == one minute
        see for possible values:
        https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases

    Returns
    -------
    pd.DataFrame
        Contains the cleaned data
    """
    # if start_datetime is None take the first datapoint
    if not start_datetime: start_datetime = dataframe.index[0]
    # if end_datetime is None take the last datapoint
    if not end_datetime: end_datetime = dataframe.index[-1]

    return dataframe.reindex(
        index=pd.date_range(start_datetime, end_datetime, freq=freq),
        method=None)


def detect_invalid_values(dataframe, min_val, max_val):
    """Detect erroneous datapoints and replace them by n/a

    Values that are considered as invalid (CLIENT REQUIREMENT):
    - too low
    - too high

    Parameters
    ----------
    dataframe : pd.DataFrame
        Dataframe containing a timeseries (index == pd.DatetimeIndex)
    min_val : dict(str, float)
        Mapping between the datatype and it's minimum acceptable value
        e.g. {'power' : 1, ...}
    max_val : dict(str, float)
        Mapping between the datatype and it's maximum acceptable value
        e.g. {'power' : 10000, ...}

    Returns
    -------
    pd.DataFrame
        Contains the cleaned data
    """
    # search for invalid values
    too_low = dataframe < min_val
    too_high = dataframe > max_val

    # replace invalid values by nan
    return dataframe.where(~(too_low | too_high))


def detect_sunrise_sunset(dataframe, longitude, latitude):
    """Detect the first and last production values

    Consider missing rows between sunset and sunrise as 0 instead of n/a

    Clients:
    - compute the time of sunset and sunrise with a formula & add security bound

    Parameters
    ----------
    dataframe : pd.DataFrame
        Dataframe containing a timeseries (index == pd.DatetimeIndex)
    longitude : float
        The longitude of the location for which to compute sunrise and sunset
    latitude : float
        The longitude of the location for which to compute sunrise and sunset

    Returns
    -------
    pd.DataFrame
        Contains the cleaned data
    """
    # Search first valid datapoint
    first_valid_index = dataframe.first_valid_index()
    if first_valid_index:
        is_before_first_valid = dataframe.index < first_valid_index
    else:
        is_before_first_valid = False

    # Compute sunrise and sunset
    sunrise, sunset = compute_sunrise_sunset(
        dataframe.index[0].date(), dataframe.index[-1].date(),
        longitude, latitude)

    # Add a security marge
    sunrise = sunrise - np.timedelta64(SUNRISE_SECURITY_MARGE, 'm')
    sunset = sunset + np.timedelta64(SUNSET_SECURITY_MARGE, 'm')

    # Make a boolean mask that determines if its day or night
    is_between_sunrise_sunset = pd.concat([
        pd.Series(True, index=pd.DatetimeIndex(sunrise, tz=pytz.utc)),
        pd.Series(False, index=pd.DatetimeIndex(sunset, tz=pytz.utc))]
    ).sort_index().reindex(
        index=dataframe.index,
        method='ffill',
        fill_value=False)

    # Replace `n/a` by `0` where mask == False (after first valid & at night)
    return dataframe.where(
        is_before_first_valid | is_between_sunrise_sunset,
        dataframe.fillna(0))


def preprocess_production(dataframe, start_datetime, end_datetime, min_val,
                          max_val, longitude, latitude, freq='1min'):
    """Preprocess production data

    Preprocessing for production:
    - round datetimes so that seconds == 0
    - detect missing rows (insert nan instead)
    - missing rows at night are not considered as missing but as 0
    - detect invalid value

    Parameters
    ----------
    dataframe : pd.DataFrame
        Dataframe containing a timeseries (index == pd.DatetimeIndex)
    start_datetime : datetime
        Start date of the data (needed to detect missing rows at the beginning)
    end_datetime : datetime
        End date of the data (needed to detect missing rows at the end)
    min_val : dict(str, float)
        Mapping between the datatype and it's minimum acceptable value
        e.g. {'power' : 1, ...}
    max_val : dict(str, float)
        Mapping between the datatype and it's maximum acceptable value
        e.g. {'power' : 10000, ...}
    longitude : float
        The longitude of the location for which to compute sunrise and sunset
    latitude : float
        The longitude of the location for which to compute sunrise and sunset
    freq : str
        Frequency of the new index, default = '1min' == one minute

    Returns
    -------
    pd.DataFrame
        Contains the cleaned data
    """
    dataframe = round_minutes(dataframe, freq=freq)
    dataframe = remove_duplicate_rows(dataframe)
    dataframe = add_missing_rows(
        dataframe, start_datetime, end_datetime, freq=freq)
    dataframe = detect_sunrise_sunset(dataframe, longitude, latitude)
    dataframe = detect_invalid_values(dataframe, min_val, max_val)
    dataframe.index.rename('datetime', inplace=True)
    return dataframe


def preprocess_consumption(dataframe, start_datetime, end_datetime, min_val,
                           max_val, freq='1min'):
    """Preprocess consumption data

    Preprocessing for production:
    - round datetimes so that seconds == 0
    - detect missing rows (insert nan instead)
    - detect invalid value

    Parameters
    ----------
    dataframe : pd.DataFrame
        Dataframe containing a timeseries (index == pd.DatetimeIndex)
    start_datetime : datetime
        Start date of the data (needed to detect missing rows at the beginning)
    end_datetime : datetime
        End date of the data (needed to detect missing rows at the end)
    min_val : dict(str, float)
        Mapping between the datatype and it's minimum acceptable value
        e.g. {'power' : 1, ...}
    max_val : dict(str, float)
        Mapping between the datatype and it's maximum acceptable value
        e.g. {'power' : 10000, ...}
    freq : str
        Frequency of the new index, default = '1min' == one minute

    Returns
    -------
    pd.DataFrame
        Contains the cleaned data
    """
    dataframe = round_minutes(dataframe, freq=freq)
    dataframe = remove_duplicate_rows(dataframe)
    dataframe = add_missing_rows(dataframe, start_datetime, end_datetime, freq=freq)
    dataframe = detect_invalid_values(dataframe, min_val, max_val)
    dataframe.index.rename('datetime', inplace=True)
    return dataframe
