"""
Integrated software project - Electrical energy at a glance - Team 2

Functions for accessing data-sources

Author: Dominik Zians
"""

# Standard library
import os
from datetime import datetime

# Others
import sqlalchemy as sqla
import pandas as pd
import pytz


###############################################################################
# Constants                                                                   #
###############################################################################


BRUSSELS = pytz.timezone('Europe/Brussels')


###############################################################################
# SQLAlchemy engines of the data-sources                                      #
###############################################################################


elecmon_engine = sqla.create_engine(
    f'mysql://{os.environ["ELECMON_USER"]}'
    f':{os.environ["ELECMON_PASSWD"]}'
    f'@{os.environ["ELECMON_HOST"]}'
    f'/{os.environ["ELECMON_DATABASE"]}')

solar_engine = sqla.create_engine(
    f'mysql://{os.environ["SOLAR_USER"]}'
    f':{os.environ["SOLAR_PASSWD"]}'
    f'@{os.environ["SOLAR_HOST"]}'
    f'/{os.environ["SOLAR_DATABASE"]}')

# <- HERE YOU CAN ADD INFORMATION OF NEW SOURCE DATABASES


###############################################################################
# Access function generators                                                  #
###############################################################################


def make_single_access(engine, query, timezone):
    """Create an access function to one building

    Parameters
    ----------
    engine : sqlalchemy engine
        The connection to the SQL-server
    query : str
        The query that is first formated then executed
        This query must have 2 keyword arguments: `start_date`, and `end_date`
        which can be replaced by the format function
        see https://docs.python.org/3.8/library/string.html#format-string-syntax
        example:
        "SELECT * FROM db.b28 WHERE datetime BETWEEN '{start_date}' AND '{end_date}'"
    timezone : pytz.timezone
        The timezone of the source data

    Returns
    -------
    function(start_date, end_date)
    """
    # raise KeyError if the 2 required arguments are not present
    query.format(start_date='', end_date='')

    # Create the return function
    def single_access(start_date, end_date):
        """Get raw data from an external source

        Datetime of the source is converted to UTC

        Parameters
        ----------
        start_date : datetime
            The time of the first datapoint
            If it contains timezoneinfo it is converted to the local time
        end_date : datetime
            The time of the last datapoint
            If it contains timezoneinfo it is converted to the local time

        Returns
        -------
        pd.DataFrame
        """
        # Check all the parameters
        if not isinstance(start_date, datetime):
            raise ValueError('Invalid start_date')
        if not isinstance(end_date, datetime):
            raise ValueError('Invalid end_date')

        # Convert to correct timezone
        if start_date.tzinfo is not None:
            start_date = start_date.astimezone(timezone)
        if start_date.tzinfo is not None:
            end_date = end_date.astimezone(timezone)

        # Format the query
        query_formatted = query.format(
            start_date=start_date,
            end_date=end_date)

        # Extract the data
        data = pd.read_sql(query_formatted, engine, index_col='datetime')

        # Convert the index into a DatetimeIndex in UTC
        # see https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DatetimeIndex.html
        data.index = pd.DatetimeIndex(
            data.index,
            tz=timezone,
            # Because we order by id in the sql query, the order of the rows
            # corresponds to the insertion order. When going from 03:00 DST to
            # 02:00 non-DST we want to mark the first rows as True (signifies 
            # DST). duplicated() will mark the first duplicate as True with
            # keep='last'
            ambiguous=data.index.duplicated(keep='last')
        ).tz_convert(
            pytz.utc)

    return single_access


def make_multi_access(engine, query, possible_buildings, timezone):
    """Create an access function to multiple buildings

    Parameters
    ----------
    engine : sqlalchemy engine
        The connection to the SQL-server
    query : str
        The query that is first formated then executed
        This query must have 3 keyword arguments: `building`, `start_date`, and
        `end_date` that can be replaced by the format function
        see https://docs.python.org/3.8/library/string.html#format-string-syntax
        example:
        "SELECT * FROM db.{building} WHERE datetime BETWEEN '{start_date}' AND '{end_date}'"
        This query must return a column named `datetime`, other columns should
        be renamed to match the datatypes of the NYK-API ('AS' sql-operator)
    possible_buildings : set(str)
        A set containing all buildings supported by the access
    timezone : pytz.timezone
        The timezone of the source data

    Returns
    -------
    function(building, start_date, end_date)
    """
    # raise KeyError if the 3 required arguments are not present
    query.format(building='', start_date='', end_date='')

    # Create the return function
    def multi_access(building, start_date, end_date):
        """Get raw data from an external source

        Datetime of the source is converted to UTC

        Parameters
        ----------
        building : str
            The name of the building
        start_date : datetime
            The time of the first datapoint
            If it is a "timzone aware datatime" it is converted to local time
            of the data source, otherwise it is interpreted as local time
        end_date : datetime
            The time of the last datapoint
            If it is a "timzone aware datatime" it is converted to local time
            of the data source, otherwise it is interpreted as local time

        Returns
        -------
        pd.DataFrame
        """
        # Check all the parameters
        if building not in possible_buildings:
            raise ValueError('Invalid building')
        if not isinstance(start_date, datetime):
            raise ValueError('Invalid start_date')
        if not isinstance(end_date, datetime):
            raise ValueError('Invalid end_date')

        # Convert to correct timezone
        if start_date.tzinfo is not None:
            start_date = start_date.astimezone(timezone)
        if start_date.tzinfo is not None:
            end_date = end_date.astimezone(timezone)

        # Format the query
        query_formatted = query.format(
            building=building,
            start_date=start_date,
            end_date=end_date)

        # Extract the data
        data = pd.read_sql(query_formatted, engine, index_col='datetime')

        # Convert the index into a DatetimeIndex in UTC
        # see https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DatetimeIndex.html
        data.index = pd.DatetimeIndex(
            data.index,
            tz=timezone,
            # Because we order by id in the sql query, the order of the rows
            # corresponds to the insertion order. When going from 03:00 DST to
            # 02:00 non-DST we want to mark the first rows as True (signifies 
            # DST). duplicated() will mark the first duplicate as True with
            # keep='last'
            ambiguous=data.index.duplicated(keep='last')
        ).tz_convert(
            pytz.utc)
        return data

    return multi_access


###############################################################################
# Access functions for getting raw data from extern sources                   #
###############################################################################

# Queries should rename columns with the 'AS' sql-operator to match the NYK-API

get_raw_elecmon = make_multi_access(
    elecmon_engine,
    '''
        SELECT
            Date AS datetime,
            TensionV12 AS voltage_12,
            TensionV23 AS voltage_23,
            TensionV31 AS voltage_31,
            I1 AS current_1,
            I2 AS current_2,
            I3 AS current_3,
            TotalkW AS active_power,
            TotalkVA AS reactive_power,
            TotalCosPhi AS cos_phi
        FROM elecmon.satec_{building}_min
        WHERE Date BETWEEN '{start_date}' AND '{end_date}'
        ORDER BY id;
    ''',
    {'b28', 'b52'},
    BRUSSELS)

get_raw_solar = make_multi_access(
    solar_engine,
    '''
        SELECT
            DateTime AS datetime,
            Power_{building} /1000 AS power
        FROM Solaire.parkings
        WHERE DateTime BETWEEN '{start_date}' AND '{end_date}'
        ORDER BY id;
    ''',
    {f'{i+1}' for i in range(8)},
    BRUSSELS)

# <- HERE YOU CAN ADD NEW DATA ACCESSES


###############################################################################
# Individual handlers for each building                                       #
###############################################################################


handlers = {
    'B28': lambda s, e: get_raw_elecmon('b28', s, e),
    'B52': lambda s, e: get_raw_elecmon('b52', s, e),
    'Parking1': lambda s, e: get_raw_solar('1', s, e),
    'Parking2': lambda s, e: get_raw_solar('2', s, e),
    'Parking3': lambda s, e: get_raw_solar('3', s, e),
    'Parking4': lambda s, e: get_raw_solar('4', s, e),
    'Parking5': lambda s, e: get_raw_solar('5', s, e),
    'Parking6': lambda s, e: get_raw_solar('6', s, e),
    'Parking7': lambda s, e: get_raw_solar('7', s, e),
    'Parking8': lambda s, e: get_raw_solar('8', s, e)
}
