"""
Integrated software project - Electrical energy at a glance - Team 2

Main program of the data_integration microservice

Author: Dominik Zians
"""

# Own modules
import data_update as du
import data_sources as ds

# Standard library
import os
import time
import threading
import logging
import sys

# Others
import schedule


###############################################################################
# Configuration                                                               #
###############################################################################


logging.basicConfig(
    format='%(asctime)s | %(levelname)s | %(message)s',
    level=logging.INFO,
    handlers=[
        logging.FileHandler("data_integration.log"),
        logging.StreamHandler(sys.stdout)
    ])

TOKEN_UPDATE_INTERVAL = 55  # in minutes
DATA_UPDATE_INTERVAL = 1  # in minutes


###############################################################################
# Helper functions                                                            #
###############################################################################


def run_threaded(*args):
    """Run a job in a new thread

    see https://schedule.readthedocs.io/en/stable/faq.html#how-can-i-make-sure-long-running-jobs-are-always-executed-on-time
    """
    job_thread = threading.Thread(target=args[0], args=args[1:])
    job_thread.start()


def test_job():
    print("I'm running on thread %s" % threading.current_thread())


def log_exception(*args):
    """Execute a function, catch and log any unhandled exception

    Just to be sure that the main program doesn't crash
    """
    try:
        args[0](*args[1:])
    except:
        logging.exception('Unhandled error occurred:')


###############################################################################
# Main program                                                                #
###############################################################################


if __name__ == '__main__':
    # Log beginning
    logging.info(f'Starting the data-integration-microservice')

    # Initialize access to the NYK-APIs
    api_handler = du.ApiHandler()

    # Initialize the buildings
    buildings = api_handler.get_buildings(ds.handlers)

    if buildings:
        # Log success of initialization
        logging.info('Buildings initialized')

        # Schedule all the jobs:
        # - Update the token before it expires
        schedule.every(TOKEN_UPDATE_INTERVAL).minutes.do(
            run_threaded,
            log_exception,
            api_handler.update_token)

        # - Update all buildings
        for building in buildings:
            # execute one time
            building.update_data(api_handler)

            # schedule automatic update
            schedule.every(DATA_UPDATE_INTERVAL).minutes.do(
                run_threaded,
                log_exception,
                building.update_data, 
                api_handler)

        # Execute jobs
        while True:
            schedule.run_pending()
            time.sleep(1)

    else:
        # No building initialized
        logging.critical('Could not initialize buildings')