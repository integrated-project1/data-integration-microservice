"""
Integrated software project - Electrical energy at a glance - Team 2

Functions for data-analysis with pandas, not part of the main program

Author: Dominik Zians
"""

# Own modules
import data_sources as ds
import data_integration as di

# Standard library
from datetime import datetime, time

# Others
import pandas as pd

# !!! For using this module you need to have matplotlib installed it is not in
# requirements.txt because is module is not used in the final program
import matplotlib.pyplot as plt


def plot_consumption(df):
    """Plot a consumption dataframe

    Parameters
    ----------
    df : pd.DataFrame
        The dataframe containing the data to plot
    """
    _, axs = plt.subplots(2, 2, sharex=True)
    df[['voltage_12', 'voltage_23', 'voltage_31']].plot(ax=axs[0][0])
    df[['current_1', 'current_2', 'current_3']].plot(ax=axs[0][1])
    df[['active_power', 'reactive_power']].plot(ax=axs[1][0])
    df['cos_phi'].plot(ax=axs[1][1])
    plt.show()


def plot_production(df):
    """Plot a production dataframe

    Parameters
    ----------
    df : pd.DataFrame
        The dataframe containing the data to plot
    """
    _, axs = plt.subplots(1)
    df['power'].plot(ax=axs)
    plt.show()


def boxplot_consumption(df):
    """Plot a consumption dataframe

    Parameters
    ----------
    df : pd.DataFrame
        The dataframe containing the data to plot
    """
    _, axs = plt.subplots(2, 2)
    df[['voltage_12', 'voltage_23', 'voltage_31']].boxplot(ax=axs[0][0])
    df[['current_1', 'current_2', 'current_3']].boxplot(ax=axs[0][1])
    df[['active_power', 'reactive_power']].boxplot(ax=axs[1][0])
    df['cos_phi'].boxplot(ax=axs[1][1])
    plt.show()


def boxplot_production(df):
    """Plot a production dataframe

    Parameters
    ----------
    df : pd.DataFrame
        The dataframe containing the data to plot
    """
    _, axs = plt.subplots(1)
    df['power'].plot(ax=axs)
    plt.show()


def show_metrics(df):
    print(
        "min:", df.min(),
        "max:", df.max(),
        "mean:", df.mean(),
        "std:", df.std(),
        sep='\n------------------------------\n')


if __name__ == '__main__':
    for building, handler in ds.handlers.items():
        print(f'####################\n{building}\n####################')

        df = handler(
            datetime(2019, 8, 1, 0, 0),
            datetime(2020, 7, 31, 23, 59))

        if building in {'B28', 'B52'}:
            df_prep = di.preprocess_consumption(
                df, None, None,
                min_val = {  # very loose minima
                    'voltage_12': 14500,
                    'voltage_23': 14500,
                    'voltage_31': 14500,
                    'current_1': 0,
                    'current_2': 0,
                    'current_3': 0,
                    'active_power': 0,
                    'reactive_power': 0,
                    'cos_phi': 0.5},
                max_val = {  # very loose maxima
                    'voltage_12': 16500,
                    'voltage_23': 16500,
                    'voltage_31': 16500,
                    'current_1': 100,
                    'current_2': 100,
                    'current_3': 100,
                    'active_power': 1000,
                    'reactive_power': 1000,
                    'cos_phi': 1.0})
            #plot_consumption(df)
            plot_consumption(df_prep)
        else:
            df_prep = di.preprocess_production(
                df, None, None,
                {'power': 0},  # very loose minima
                {'power': 150},  # very loose maxima
                5.557556, 50.585354)
            #plot_production(df)
            plot_production(df_prep)

        show_metrics(df_prep)
