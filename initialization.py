"""
Integrated software project - Electrical energy at a glance - Team 2

Bring the database in an initial state

Author: Dominik Zians
"""

# Own modules
import data_update as du
from data_client.openapi_client import BuildingInfo

# Standart library
from datetime import datetime
import logging
import sys

# Others
import pytz


###############################################################################
# Configuration                                                               #
###############################################################################



from configuration import MAX_DATE


logging.basicConfig(
    format='%(asctime)s | %(levelname)s | %(message)s',
    level=logging.INFO,
    handlers=[
        logging.FileHandler("data_initialization.log"),
        logging.StreamHandler(sys.stdout)
    ])

buildings = [
    BuildingInfo(
        name='B28',
        type='consumption',
        location=[50.586200, 5.560500],
        display_name='B28, B37, B65 and B26',
        map_datatype='active_power',
        datatypes=[
            'voltage_12', 'voltage_23', 'voltage_31',
            'current_1', 'current_2', 'current_3',
            'active_power', 'reactive_power', 'cos_phi'],
        maxima=[16000, 16000, 16000, 30, 30, 30, 700, 700, 1],
        minima=[15000, 15000, 15000, 0, 0, 0, 0, 0, 0.8]),
    BuildingInfo(
        name='B52',
        type='consumption',
        location=[50.585354, 5.557556],
        display_name='B52, B53 and B48',
        map_datatype='active_power',
        datatypes=[
            'voltage_12', 'voltage_23', 'voltage_31',
            'current_1', 'current_2', 'current_3',
            'active_power', 'reactive_power', 'cos_phi'],
        maxima=[16500, 16500, 16500, 25, 25, 25, 700, 700, 1],
        minima=[14500, 14500, 14500, 0, 0, 0, 0, 0, 0.5]),
    BuildingInfo(
        name='Parking1',
        type='production',
        location=[50.585756, 5.568083],  
        display_name='Parking photovoltaics 1', 
        map_datatype='power',
        datatypes=['power'],
        maxima=[100],
        minima=[0]),
    BuildingInfo(
        name='Parking2',
        type='production',
        location=[50.585556, 5.568130],
        display_name='Parking photovoltaics 2',
        map_datatype='power',
        datatypes=['power'],
        maxima=[100],
        minima=[0]),
    BuildingInfo(
        name='Parking3',
        type='production',
        location=[50.585823, 5.568482], 
        display_name='Parking photovoltaics 3',
        map_datatype='power',
        datatypes=['power'],
        maxima=[100],
        minima=[0]),
    BuildingInfo(
        name='Parking4',
        type='production',
        location=[50.585633, 5.568551],
        display_name='Parking photovoltaics 4',
        map_datatype='power',
        datatypes=['power'],
        maxima=[100],
        minima=[0]),
    BuildingInfo(
        name='Parking5',
        type='production',
        location=[50.585904, 5.568887],
        display_name='Parking photovoltaics 5',
        map_datatype='power',
        datatypes=['power'],
        maxima=[30],
        minima=[0]),
    BuildingInfo(
        name='Parking6',
        type='production',
        location=[50.585716, 5.568962],
        display_name='Parking photovoltaics 6',
        map_datatype='power',
        datatypes=['power'],
        maxima=[30],
        minima=[0]),
    BuildingInfo(
        name='Parking7',
        type='production',
        location=[50.585994, 5.569358],
        display_name='Parking photovoltaics 7',
        map_datatype='power',
        datatypes=['power'],
        maxima=[30],
        minima=[0]),
    BuildingInfo(
        name='Parking8',
        type='production',
        location=[50.585821, 5.569441],
        display_name='Parking photovoltaics 8',
        map_datatype='power',
        datatypes=['power'],
        maxima=[30],
        minima=[0]),
]


###############################################################################
# Main program                                                                #
###############################################################################


if __name__ == '__main__':
    # Log beginning
    logging.info(f'Starting the data-api initialization')

    # Initialize access to the NYK-APIs
    api_handler = du.ApiHandler()
    logging.info('ApiHandler initialized')

    for building in buildings:
        # If last_added is set for a building, but no data has been added
        # data_update will use last_added as first date for the update
        building.last_added = MAX_DATE

        # Add building to the API
        api_handler.add_building(building)

    logging.info(f'Finished the data-api initialization')
