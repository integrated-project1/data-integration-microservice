# Integrated software project - Electrical energy at a glance - Team 2

# Use the base image containing already pandas
FROM registry.gitlab.com/integrated-project1/the-main-api/pandas-alpine

RUN apk add --no-cache openssl

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

# Create work directory
RUN mkdir -p /usr/src/app /usr/scr/app/auth_client /usr/scr/app/data_client
WORKDIR /usr/src/app

# Getting the auth client & installing requirements
COPY --from=registry.gitlab.com/integrated-project1/authentication-microservice/client:master /usr/src/app/openapi_client ./auth_client/openapi_client
COPY --from=registry.gitlab.com/integrated-project1/authentication-microservice/client:master /usr/src/app/requirements.txt ./auth_client/openapi_client/requirements.txt
RUN pip3 install --no-cache-dir -r ./auth_client/openapi_client/requirements.txt

# Getting the data client & installing requirements
COPY --from=registry.gitlab.com/integrated-project1/the-main-api/python-client:master /usr/src/app/openapi_client ./data_client/openapi_client
COPY --from=registry.gitlab.com/integrated-project1/the-main-api/python-client:master /usr/src/app/requirements.txt ./data_client/openapi_client/requirements.txt
RUN pip3 install --no-cache-dir -r ./data_client/openapi_client/requirements.txt

# Why can't we change a client's name at codegen...
RUN touch ./auth_client/__init__.py ./data_client/__init__.py \
    && find ./auth_client -type f -name '*.py' | xargs sed -i 's~openapi_client~auth_client.openapi_client~g' \
    && find ./data_client -type f -name '*.py' | xargs sed -i 's~openapi_client~data_client.openapi_client~g'

# Install requirements and dependecies for data-integration
COPY requirements.txt requirements.txt
RUN apk add --no-cache --virtual build-deps gcc musl-dev python3-dev \
    && apk add mariadb-dev \
    && pip install --no-cache-dir -r requirements.txt \
    && apk del python3-dev build-deps

# Copy all other files
COPY . /usr/src/app

# Start periodiaclly updating the database
CMD [ "python", "-u", "main.py" ]
