"""
Integrated software project - Electrical energy at a glance - Team 2

Module containing classes to support continuous data update

Author: Dominik Zians
"""

# Own modules
import data_integration as di

import data_client.openapi_client as dc
from data_client.openapi_client.rest import ApiException as DataApiException

import auth_client.openapi_client as ac
from auth_client.openapi_client.rest import ApiException as AuthApiException

# Standart library
import os
import threading
import logging
from urllib3.exceptions import MaxRetryError
from datetime import datetime, timedelta

# Other
import pandas as pd
from sqlalchemy.exc import OperationalError
import pytz


###############################################################################
# Utils                                                                       #
###############################################################################


def api_data_to_dataframe(api_data):
    """Convert a dc.Data object into a pd.DataFrame

    Parameters
    ----------
    api_data : dc.Data
        A data object received from the API-client

    Returns
    -------
    pd.DataFrame
        The api_data as a DataFrame
    """
    return pd.DataFrame(
        api_data.data,
        columns=api_data.columns,
        index=pd.DatetimeIndex(api_data.index))


###############################################################################
# Class to handle both APIs and the token mechanism                           #
###############################################################################


class BuildingValueError(ValueError):
    """Class to differentiate an ValueError raised by the Building"""
    pass


class ApiHandler():
    """Class bundling both NYK-APIs

    The data-integration module uses primarily API-call that require an
    access token, this class aim at facilitating the authentication via a
    token and periodical update of the token because they are only valid for
    a certain time.
    """
    def __init__(self):
        """Initialise an API-handler"""
        self.data_api_config = dc.Configuration(
            host=f"http://{os.environ['DATA_API_HOST']}"
                 f":{os.environ['DATA_API_PORT']}/1.0.0")
        self.auth_api_config = ac.Configuration(
            host=f"http://{os.environ['AUTH_API_HOST']}"
                 f":{os.environ['AUTH_API_PORT']}/1.0.0")
        self.lock = threading.RLock()
        self.update_token()

        logging.debug('ApiHandler: Initialized')

    def get_buildings(self, handlers):
        """Get a list of buildings from the API
 
        Parameters
        ----------
        handlers : dict(str, func)
            Dictionary containing data access functions

        Returns
        -------
        list(Building)
        """

        logging.info('ApiHandler: Start initialization of buildings')

        with self.lock, dc.ApiClient(self.data_api_config) as api_client:
            # Create an instance of the API class
            api_instance = dc.DefaultApi(api_client)

            try:
                # Get a list of buildings from the building-api
                api_buildings = api_instance.get_buildings()
                logging.debug(
                    f'ApiHandler: List of buildings:\n{api_buildings}')

            except MaxRetryError:
                # Could not connect to the data-API
                logging.exception(
                    f'ApiHandler: Could NOT connect to the data-API')
                return []

            except DataApiException:
                # Could not access the building-list
                logging.exception(
                    f'ApiHandler: Could NOT retrieve the list of buildings')
                return []

            # Retrieve information, about the buildings where a handler exists
            buildings = []
            for building in api_buildings:
                # Check if a handler is available
                if building not in handlers:
                    logging.error(
                        'ApiHandler: Missing raw data handler for '
                        f'"{building}"')
                    continue

                try:
                    # Retrieve all the needed information
                    info = api_instance.get_building_info(building)
                    logging.debug(f'ApiHandler: "{building}": Info:\n{info}')

                    last_datapoint = api_data_to_dataframe(
                        api_instance.get_data(
                            building,
                            info.last_added,
                            info.last_added))
                    logging.debug(
                        f'ApiHandler: "{building}": last_datapoint:\n'
                        f'{last_datapoint}')

                    if len(last_datapoint) == 0:
                        # no data has been added yet
                        last_datapoint = info.last_added

                    # Create the building
                    buildings.append(Building(
                        building,
                        info.type,
                        handlers[building],
                        info.datatypes,
                        info.minima,
                        info.maxima,
                        info.location[1],
                        info.location[0],
                        last_datapoint))

                except DataApiException:
                    # Could not access the building-information
                    logging.exception(
                        f'ApiHandler: "{building}": '
                        'Could NOT retrieve information')

                except BuildingValueError:
                    # Building with a wrong type
                    logging.exception(
                        f'ApiHandler: "{building}": Invalid type')

                except ValueError:
                    # read_json
                    logging.exception(
                        f'ApiHandler: "{building}": '
                        'Error while reading json into dataframe')

        return buildings

    def update_token(self):
        """Request a new token from the authentication api

        Since a token is only valid for a certain time it has to be updated
        periodically. The lock is used to prevent that the token is changed
        while an other thread uses tha data-api.
        """

        logging.info(f'ApiHandler: Start access token update')

        with ac.ApiClient(self.auth_api_config) as api_client:
            # Create an instance of the API class
            auth_api_instance = ac.DefaultApi(api_client)

            # Create info for login
            authentication_info = ac.AuthenticationInfo(
                os.environ['API_USER'],
                os.environ['API_PASSWD'],
                response_type='token')

            # Get the access token
            try:
                login = auth_api_instance.login(authentication_info)
                api_token = login[2]['Location']\
                    .split('#')[1].split('&')[0].split('=')[1]

            except MaxRetryError:
                # Could not connect to the data-API
                logging.exception(
                    f'ApiHandler: Could NOT connect to the auth-API')

            except AuthApiException:
                logging.exception(
                    f'ApiHandler: Could NOT update the access token!')

        with self.lock:
            # Update the access token
            self.data_api_config.access_token = api_token
        logging.info(f'ApiHandler: Access token update successful')

    def post_data(self, building, dataframe):
        """Post data through the data api

        Parameters
        ----------
        building : str
            The building
        dataframe : pandas.DataFrame
            The data
        """
        with self.lock, dc.ApiClient(self.data_api_config) as api_client:
            # Create an instance of the API class
            api_instance = dc.DefaultApi(api_client)

            # Post a list of data points
            api_instance.post_data(
                building,
                dataframe.to_dict(orient='split'))

    def add_building(self, building):
        """Add a building to the data-api

        Parameters
        ----------
        building : dc.BuildingInfo
            The building to add
        """
        with self.lock, dc.ApiClient(self.data_api_config) as api_client:
            # Create an instance of the API class
            api_instance = dc.DefaultApi(api_client)

            # Post the building
            api_instance.add_building(building)


###############################################################################
# Class to handle a building                                                  #
###############################################################################


class Building:
    """Class containing all information needed to update a building"""
    def __init__(self, name, typ, handler, datatypes, min_val, max_val,
                 longitude, latitude, last_added):
        """Initialize a building

        Parameters:
        -----------
        name : str
            The name of the building
        typ : str
            The type of the building
        handler : callable
            The data access handler of the building
        datatypes : list(str)
            A list of available datatypes of this building
        min_val : list(float)
            The minimum possible values for each datatype
            This list must have the same length as `datatypes`
        max_val : list(float)
            The maximum possible values for each datatype
            This list must have the same length as `datatypes`
        longitude : float
            The longitude of the location of the building
        latitude : float
            The latitude of the location of the building
        last_added : pd.Dataframe
            The last row of data inserted
        """
        if typ not in {'consumption', 'production'}:
            raise BuildingValueError('Invalid type')
        self.name = name
        self.typ = typ
        self.handler = handler
        self.min_val = {dt: mi for dt, mi in zip(datatypes, min_val)}
        self.max_val = {dt: ma for dt, ma in zip(datatypes, max_val)}
        self.longitude = longitude
        self.latitude = latitude
        self.last_added = last_added
        self.lock = threading.RLock()

        logging.info(f'Building "{self.name}": Initialized')

    def update_data(self, api_handler):
        """Retrieve new data of this building

        There are two different cases for the update:
        - There is already data for this building in the API-database:
            In this case the building is already initialized and
            self.last_added is a pd.DataFrame containing the most recent
            datapoint of the building
        - There is no data for this building in the API-database:
            In this case we want to initialize the building and self.last_added
            is a datetime containing the from_date for the initialization.

        Here the lock is used to prevent double access to self.last_added

        Parameters
        ----------
        api_handler : ApiHandler
            An ApiHandler which is used to post the data
        """

        logging.info(f'Building "{self.name}": Start update')

        with self.lock:
            # Define date range. As long as timezone info is given, it
            # doesn't matter which timezone
            if isinstance(self.last_added, datetime):
                # no data added yet, last_added represents the first date for
                # the initialization
                get_from = self.last_added
                # do not detect missing values in the beginning
                prep_from = None
            else:
                # last_added datapoint already exists, therefor ask data from
                # 30 sec later to get the next datapoint
                get_from = self.last_added.index[0]\
                         + timedelta(seconds=30, microseconds=1)
                # detect missing values in the beginning
                prep_from = self.last_added.index[0]
            get_to = datetime.now(tz=pytz.utc)
            logging.debug(
                f'Building "{self.name}": From {get_from} to {get_to}')

            try:
                # Get raw data
                data = self.handler(get_from, get_to)  # returns data in UTC

                # Need to add last_added to the data,
                # otherwise detect_sunrise_sunset would not work as expected
                if isinstance(self.last_added, pd.DataFrame):
                    # DataFrame.append() returns a dataframe with DatetimeIndex
                    # only if both indexes are the same timezone!
                    # Otherwise an Index is returned which is not desirable
                    # see di.round_minutes()
                    data = self.last_added.append(data)
                logging.debug(f'Building "{self.name}": Raw data:\n{data}')

                # Preprocess data
                if self.typ == 'consumption':
                    data = di.preprocess_consumption(
                        data, prep_from, None,
                        self.min_val, self.max_val)
                else:
                    data = di.preprocess_production(
                        data, prep_from, None,
                        self.min_val, self.max_val,
                        self.longitude, self.latitude)
                logging.debug(
                    f'Building "{self.name}": Processed data:\n{data}')

                # If self.last_added had been added before remove it again
                data_to_post = data if prep_from is None else data.tail(-1)

                # If there is new data, send it to the API
                if len(data_to_post):
                    api_handler.post_data(self.name, data_to_post)

                    # Update the last_added value
                    self.last_added = data_to_post.tail(1)
                    logging.info(f'Building "{self.name}": Update successful')
                else:
                    logging.info(f'Building "{self.name}": No new data')

            except DataApiException:
                # Could not access the data-API
                logging.exception(
                    f'Building "{self.name}": Could NOT post new data!')

            except OperationalError:
                # Could not access the data-source
                logging.exception(
                    f'Building "{self.name}": Could NOT retrieve new data!')
