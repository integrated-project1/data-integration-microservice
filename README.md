# Data-integration microservice

The data-integration microservice retrieves minutely data from external sources like (elecmon & solar DB), checks for missing and erroneous data-points and cleans the data, before storing the resulting data in the NYK-database.

## Data-integration

Data-integration is done with the data analysis and manipulation tool __pandas__.

Several data-transformations will be done:
- convert datetime to UTC to avoid ambiguities due to Daylight Saving Time (DST)
- round datetime column so that seconds are equal to 0
- add missing rows filled with `n/a`
- for `Production` consider missing rows between sunset and sunrise as `0` production (instead of `n/a`)
- detect erroneous values (too high, too low) and replace them by `n/a`

## How to run the code (with docker)

N.B. Make sure you are connected to the VPN of the university, otherwise the access to the data sources won't work!

First you need to give docker the access to gitlab, so it can fetch the other modules. Type in your terminal:

```docker login registry.gitlab.com```

You will be prompted to give your username and password. Note that this user should have permission to access to the gitlab repository of this and the other microservices.

Then you need to add all passwords for data-integration in your system variables with (`secret_credentials.env` is not on the git!):

```set -a && source secret_credentials.env```

Next you can build the docker images with:

```docker-compose build```

Then you can run the containers with:

```docker-compose up```

## How to add a new data source (one or more buildings)

### 1. Add a new database

If you want to use data from a database already defined in this service you can skip this step and go directly to step 3.

To add a new database you have to provide the following information:

- __user_name__: The name of a user with read permissions.
- __secret_password__: The password associated to the user above.
- __IP_address__: Address of the sql server.
- __database_name__: The name of the database on the server containing the data.

First, add 4 variables at the end of `secret_credentials.env`, Docker will store them automatically as system variables. Here an example:

```
NEWDB_USER=***user_name***
NEWDB_PASSWD=***secret_password***
NEWDB_HOST=***IP_Address***
NEWDB_DATABASE=***database_name***
```

### 2. Create an SQLAlchemy-engine

Next, you need to create and SQLAlchemy-engine in `data_sources.py` using the information from the first step. 

Here an example, note that the variable names from step 1 appear in the code:

```
newdb_engine = sqla.create_engine(
    f'mysql://{os.environ["NEWDB_USER"]}'
    f':{os.environ["NEWDB_PASSWD"]}'
    f'@{os.environ["NEWDB_HOST"]}'
    f'/{os.environ["NEWDB_DATABASE"]}')
```

### 3. Define an access-function

If there exists already an access function you can use, you can skip this step and go to step 4.

Next, you need to define a function containing an SQL-query for accessing the data from this new database in `data_sources.py`.

For this you can use the 2 helper functions:
- `make_single_access()`: Creates a function with 2 parameters: `start_date` and `end_date`, giving access to one building.
- `make_multi_access()`: Creates a function with 3 parameters: `building`, `start_date` and `end_date` giving access to multiple buildings.

Parameters of `make_single_access()` and `make_multi_access()`:
- engine: The SQLAlchemy connection to the SQL-server
- query: The SQL-query to retrieve the data from the database. For `make_single_access()` this query must have 2 keyword arguments: `start_date` and `end_date` that can be replaced by the Python format function (see https://docs.python.org/3.8/library/string.html#format-string-syntax). For `make_multi_access()`, this query must have one additional keyword argument (so 3 in total): `building`.
- possible_buildings (only for `make_multi_access()`): A set containing all buildings supported by the access.
- timezone: The timezone of the source data, which is needed to convert the timezone to UTC.

You must use the `AS` sql-operator to rename columns. The query must produce:
- For `Production`: 2 columns named `datetime` and `power`
- For `Consumption`: 10 columns named `datetime`, `voltage_12`, `voltage_23`, `voltage_31`, `current_1`, `current_2`, `current_3`, `active_power`, `reactive_power`, `cos_phi`

Here are two examples, note again that the engine name from step 2 appears in the code:

```
get_raw_newdb_multi = make_multi_access(
    newdb_engine,
    '''
        SELECT
            Date AS datetime,
            TensionV12 AS voltage_12,
            TensionV23 AS voltage_23,
            TensionV31 AS voltage_31,
            I1 AS current_1,
            I2 AS current_2,
            I3 AS current_3,
            TotalkW AS active_power,
            TotalkVA AS reactive_power,
            TotalCosPhi AS cos_phi
        FROM elecmon.satec_{building}_min
        WHERE Date BETWEEN '{start_date}' AND '{end_date}'
    ''',
    {'new_building_1', 'new_building_2'},
    BRUSSELS)
```

```
get_raw_newdb_single = make_single_access(
    newdb_engine,
    '''
        SELECT
            DateTime AS datetime,
            Power_4 AS power
        FROM SolarEnergy.parkings
        WHERE DateTime BETWEEN '{start_date}' AND '{end_date}'
    ''',
    BRUSSELS)
```

### 4. Add a handler for each building

As a last step, you need to add the access-function from step 3 to the `handlers`-dictionary in `data_sources.py`. Add for each building an entry in the dictionary, mapping the `building-name` to a `function` with exactly 2 parameters: `start_date` and `end_date`.

In the example below, lambda expressions are used to reduce the number of parameters of a mutli-access-function:

```
handlers = {
    # <- other handlers are already defined here

    # new handlers:
    'new_building_1': lambda s, e: ds.get_raw_newdb_multi('new_building_1', s, e),
    'new_building_2': lambda s, e: ds.get_raw_newdb_multi('new_building_2', s, e),
    'new_building_3': ds.get_raw_newdb_single
}
```

After all these steps the data-integration micro-service has to be restarted.

IMPORTANT: Note that these steps are only to define the data-access handlers, you still need to add a building to the API via the admin form on the NYK-website.

N.B. You can also create you own custom handler function that does not rely on a SQL-database. Every function with exactly 2 parameters (`start_date` and `end_date`) that returns a pandas.DataFrame with a pandas.DatetimeIndex in UTC is suitable for the `handlers`-dictionary. The possibilities are limitless!

## Database

Two different database models have been tested (not_used_anymore_in_this_repo/database_performance_test.py):
- Database with "Single Table Inheritance" (not_used_anymore_in_this_repo/database_single.py)
- Database with "Concrete Table Inheritance" (not_used_anymore_in_this_repo/database_split.py)

The "Concrete Table Inheritance" model uses __less memory__ while being __faster__ for insertion and retrieval.

## Input from reviews

### Sprint 0

- Would be nice to consider Python (which comes with Pandas for large dataset handling) ✅ -> we use python in general and pandas in this modules
- Why do you want to discard « very old data » (the risk of being out of memory, nowadays, does not hold)? How do you define « very old »? ✅ -> we don't discard old data
- How large will be the amount of data? ✅ -> one year of preprocessed data (2 consumption & 8 production buildings) = 220 Mb 
- Make sure to check the integrity and the trust in data when exchanging between sensors and server (secure connection) ✅ -> elecmon SQL-server does not support SSL encryption

### Sprint 1

- How do you ensure secure connection between the backbone and sensors? ✅ -> elecmon SQL-server does not support SSL encryption

### Sprint 2

- The report lacks of information on the database (a schema presenting the content of tables and their relationship) ✅ -> explained in detail in the report
- Because you have a lot of extra budget, you could be much more ambitious in the next phase (e.g., providing modules for predicting electrical consumption in the future based on existing historical data) ✅ -> Module is expandable with other preprocessing steps, ML programs can retrieve clean data from the API
- Explain how you deal with missing data, erroneous data and outliers. ✅ -> Different preprocessing steps explained in the report
- We would like to see: processing and data separation, user error management, exception handling, logging, documentation inside and outside code, proper code styling, files and directories with meaningful names and sound. More generally, you should pay attention to anything that would take your piece of software from its current state to an actual solution the customer would want to operate and maintain. -> __GENERAL__: respect coding style (✅ -> pep8 & docstring), use exceptions (✅) and logging (✅)
- Also, answer to these questions: what if the customer wants to add new buildings in the system? What if some data is missing or incoherent? ✅ -> adding a new data-source is pretty straight forward using `make_single_access` and `make_multi_access`

### Sprint 3

- You have 6 tables for NYK DBs. What we observe is that the content of tables B* is pretty much the same, whatever the tables. Is it really reasonable to have 4 tables for that? Why not a single table with some ID allowing to identify the building? Further, the content of the (potentially single) table could be exported to external tables (tenions* in a Tension table, *THD in a THD table, etc.) ✅ -> we put data in as few tables as possible
- Using your website, one can see for the production graph that every night there are periods with missing data. Obviously, during the night, the solar panels are not producing energy, and thus the data is not missing. This is the kind of elements that clearly shows that your solution is over simplistic regarding the problem you had to solve. Having some data manipulation in your server, which is so far a "dumb" proxy, would have allowed you to solve this problem as well as the next. If you display a period starting at night, there is no display of the "missing data" line, since your script fails at detecting the starting point of the "problematic" section. Web browser freezes when exporting large amount of data ✅ -> several data-preprocessing steps have been discussed with the clients and have been implemented in `data_integration.py` and deployed server side
- Python code is still lacking many good practices regarding styling and documentation. -> __GENERAL__: respect coding style

### Final review

- Your final solution is slow, has a lot of bugs (as stated in review 3), doesn't scale and still offers a poor user experience. ... More importantly, it doesn't deliver the expected features, like missing data detection and management. On this specific point, your customers know it as well, as they didn't accepted the feature in the acceptance sheets. Looking at your solution, it is also very clear that it cannot deliver such a feature since there is no processing on the server side, which was the only context in which you could actually have enabled this feature. ✅ -> this microservice aims at resolving the missing data detection and management using pandas (which should be quite fast)

### Resit review

- Regarding your database, we expect you to use a single table containing data from every building. If there has to be some minor differences from a building to another, you will need to use an inheritance strategy. If you want to split data only for performances or convenience purpose, use views. ✅ -> explained in detail in the report the 2 different explored inheritance strategies
- In your report, you will explain the motivations behind your choices and bring evidences that your solution is maintainable and can easily be enriched with new features. -> __GENERAL__: make the module so that ML could be added as feature ✅ -> it is possible to use some sort of ML as preprocessing step (e.g. detect improbable values), additionally, the API contains already preprocessed data, so the preprocessing step of possible ML algorithms is already done.
- The feature that needs your attention the most is the missing data management. The detection process must be implemented on your server as it is the only way to avoid the possibility of being dependent of data outside of a given display window. Specific attention is needed regarding production data as they behave differently. Once detection is achieved, it needs to be communicated to the client. ✅ -> several data-preprocessing steps have been discussed with the clients and have been implemented in `data_integration.py` these steps will be executed server side before data is inserted into the NYK-database
- Data export should likely be handled on the server as well since it freezes the client so far, which is not acceptable. ✅ -> the API has an .csv option
- Since you will increase the load of the server, it is important to make sure its performances remain acceptable. -> __GENERAL__: write efficient code