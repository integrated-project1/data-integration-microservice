
from datetime import datetime

import pytz

#I made this file so that i can change it with a docker config



# Define a maximum first datapoint
# To get the whole data simply set MAX_DATE to 2018-01-01 00:00
# for B28, B52 and parkings is no data before
MAX_DATE = datetime(2020, 8, 1, 0, 0, tzinfo=pytz.utc)
