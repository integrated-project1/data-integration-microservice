"""
Integrated software project - Electrical energy at a glance - Team 2

Unit tests of the data_sources module

Author: Dominik Zians
"""

# Own modules
import data_sources as ds

# Standart library
from datetime import datetime

# Others
import pytest
import pytz


def test_elecmon():
    """Test the connection, column names and if the data is valid"""
    start_datetime = datetime(2020, 1, 1, 0, 0, tzinfo=pytz.utc)
    end_datetime = datetime(2020, 1, 1, 1, 0, tzinfo=pytz.utc)

    for building in ('b28', 'b52'):
        data = ds.get_raw_elecmon(
            building, start_datetime, end_datetime)

        expected_columns = [
            'voltage_12', 'voltage_23', 'voltage_31',
            'current_1', 'current_2', 'current_3',
            'active_power', 'reactive_power', 'cos_phi']

        # Check for the right column names
        assert sorted(data.columns) == sorted(expected_columns)

        # Check that only dates in the right interval are present
        assert (data.index >= start_datetime).all()
        assert (data.index <= end_datetime).all()


def test_elecmon_invalid_building():
    """Test if an exception is raised when passing an invalid building"""
    with pytest.raises(ValueError):
        assert ds.get_raw_elecmon(
            'invalid building',
            datetime(2020, 1, 1, 0, 0),
            datetime(2020, 1, 1, 1, 0))


def test_solar():
    """Test the connection, column names and if the data is valid"""
    start_datetime = datetime(2020, 1, 1, 0, 0, tzinfo=pytz.utc)
    end_datetime = datetime(2020, 1, 1, 1, 0, tzinfo=pytz.utc)

    for building in (f'{i+1}' for i in range(8)):
        data = ds.get_raw_solar(
            building, start_datetime, end_datetime)

        # Check for the right column names
        assert data.columns == ['power']

        # Check that only dates in the right interval are present
        assert (data.index >= start_datetime).all()
        assert (data.index <= end_datetime).all()


def test_solar_invalid_building():
    """Test if an exception is raised when passing an invalid building"""
    with pytest.raises(ValueError):
        assert ds.get_raw_solar(
            'invalid building',
            datetime(2020, 1, 1, 0, 0),
            datetime(2020, 1, 1, 1, 0))
