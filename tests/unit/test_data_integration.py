"""
Integrated software project - Electrical energy at a glance - Team 2

Unit tests of the data_integration module

Author: Dominik Zians
"""

# Own modules
import data_integration as di

# Standart library
from datetime import datetime

# Other
import pandas as pd
from numpy import nan
import logging
import pytz


def test_round_minutes_without_args():
    """Test if all the minutes are rounded"""

    df_input = pd.DataFrame(
        data={'test': [1, 2, 3, 4]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 20, 12, 900),
            datetime(2020, 1, 2, 16, 21, 31),
            datetime(2020, 1, 2, 16, 22, 55, 300),
            datetime(2020, 1, 2, 16, 23, 1)]))

    df_output = di.round_minutes(df_input)

    df_expected = pd.DataFrame(
        data={'test': [1, 2, 3, 4]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 20),
            datetime(2020, 1, 2, 16, 22),
            datetime(2020, 1, 2, 16, 23),
            datetime(2020, 1, 2, 16, 23)]))

    pd.testing.assert_frame_equal(df_output, df_expected)


def test_round_minutes_freq():
    """Test if the datetime is rounded to an specified frequency"""

    df_input = pd.DataFrame(
        data={'test': [1, 2, 3, 4]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 3, 12, 900),
            datetime(2020, 1, 2, 16, 12, 31),
            datetime(2020, 1, 2, 16, 35, 55, 300),
            datetime(2020, 1, 2, 16, 56, 1)]))

    df_output = di.round_minutes(df_input, freq='15min')

    df_expected = pd.DataFrame(
        data={'test': [1, 2, 3, 4]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 0),
            datetime(2020, 1, 2, 16, 15),
            datetime(2020, 1, 2, 16, 30),
            datetime(2020, 1, 2, 17, 0)]))

    pd.testing.assert_frame_equal(df_output, df_expected)


def test_remove_duplicate_rows():
    """Test if duplicated rows are removed by making a mean of them"""
    df_input = pd.DataFrame(
        data={'test': [1, 2, 3, 4]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 20),
            datetime(2020, 1, 2, 16, 22),
            datetime(2020, 1, 2, 16, 23),
            datetime(2020, 1, 2, 16, 23)]))

    df_output = di.remove_duplicate_rows(df_input)

    df_expected = pd.DataFrame(
        data={'test': [1, 2, 3.5]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 20),
            datetime(2020, 1, 2, 16, 22),
            datetime(2020, 1, 2, 16, 23)]))

    pd.testing.assert_frame_equal(df_output, df_expected)


def test_add_missing_rows_without_args():
    """Test is missing rows inside the data are detected"""
    df_input = pd.DataFrame(
        data={'test': [1, 2, 3]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 20),
            datetime(2020, 1, 2, 16, 23),
            datetime(2020, 1, 2, 16, 24)]))

    df_output = di.add_missing_rows(df_input)

    df_expected = pd.DataFrame(
        data={'test': [1, nan, nan, 2, 3]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 20),
            datetime(2020, 1, 2, 16, 21),
            datetime(2020, 1, 2, 16, 22),
            datetime(2020, 1, 2, 16, 23),
            datetime(2020, 1, 2, 16, 24)]))

    assert df_output.equals(df_expected)


def test_add_missing_rows_start():
    """Test is missing rows at the beginning and inside are detected"""

    df_input = pd.DataFrame(
        data={'test': [1, 2, 3]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 20),
            datetime(2020, 1, 2, 16, 22),
            datetime(2020, 1, 2, 16, 23)]))

    df_output = di.add_missing_rows(
        df_input, start_datetime=datetime(2020, 1, 2, 16, 18))

    df_expected = pd.DataFrame(
        data={'test': [nan, nan, 1, nan, 2, 3]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 18),
            datetime(2020, 1, 2, 16, 19),
            datetime(2020, 1, 2, 16, 20),
            datetime(2020, 1, 2, 16, 21),
            datetime(2020, 1, 2, 16, 22),
            datetime(2020, 1, 2, 16, 23)]))

    assert df_output.equals(df_expected)


def test_add_missing_rows_end():
    """Test is missing rows at the end and inside are detected"""

    df_input = pd.DataFrame(
        data={'test': [1, 2, 3]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 20),
            datetime(2020, 1, 2, 16, 22),
            datetime(2020, 1, 2, 16, 23)]))

    df_output = di.add_missing_rows(
        df_input, end_datetime=datetime(2020, 1, 2, 16, 25))

    df_expected = pd.DataFrame(
        data={'test': [1, nan, 2, 3, nan, nan]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 20),
            datetime(2020, 1, 2, 16, 21),
            datetime(2020, 1, 2, 16, 22),
            datetime(2020, 1, 2, 16, 23),
            datetime(2020, 1, 2, 16, 24),
            datetime(2020, 1, 2, 16, 25)]))

    assert df_output.equals(df_expected)


def test_add_missing_rows_freq():
    """Test if missing rows are detected for a different frequency"""

    df_input = pd.DataFrame(
        data={'test': [1, 2, 3, 4]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 0),
            datetime(2020, 1, 2, 16, 15),
            datetime(2020, 1, 2, 16, 30),
            datetime(2020, 1, 2, 17, 0)]))

    df_output = di.add_missing_rows(df_input, freq='15min')

    df_expected = pd.DataFrame(
        data={'test': [1, 2, 3, nan, 4]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 0),
            datetime(2020, 1, 2, 16, 15),
            datetime(2020, 1, 2, 16, 30),
            datetime(2020, 1, 2, 16, 45),
            datetime(2020, 1, 2, 17, 0)]))

    assert df_output.equals(df_expected)


def test_detect_invalid_values():
    """Test if invalid values are detected and replaced by nan"""
    df_input = pd.DataFrame(
        data={'test': [1, 200, 3, -1]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 2, 16, 0),
            datetime(2020, 1, 2, 16, 1),
            datetime(2020, 1, 2, 16, 2),
            datetime(2020, 1, 2, 16, 3)]))

    df_output = di.detect_invalid_values(
        df_input,
        {'test' : 1},
        {'test' : 10})

    df_expected = pd.DataFrame(
        data={'test': [1, nan, 3, nan]},
        index=df_input.index)

    pd.testing.assert_frame_equal(df_output, df_expected)


def test_detect_sunrise_sunset_first_invalid():
    df_input = pd.DataFrame(
        data={'test': [nan, nan, nan, 0, 5, 6]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 1, 7, 33),
            datetime(2020, 1, 1, 7, 34),
            datetime(2020, 1, 1, 7, 35),
            datetime(2020, 1, 1, 7, 36),
            datetime(2020, 1, 1, 7, 37),
            datetime(2020, 1, 1, 7, 38)], tz=pytz.utc))

    df_output = di.detect_sunrise_sunset(
        df_input, 5.5755752, 50.6454981)

    df_expected = pd.DataFrame(
        data={'test': [nan, nan, nan, 0, 5, 6]},
        index=df_input.index)

    pd.testing.assert_frame_equal(df_output, df_expected)


def test_detect_sunrise_sunset_first_valid():
    df_input = pd.DataFrame(
        data={'test': [0, nan, nan, 0, 5, 6]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 1, 7, 33),
            datetime(2020, 1, 1, 7, 34),
            datetime(2020, 1, 1, 7, 35),
            datetime(2020, 1, 1, 7, 36),
            datetime(2020, 1, 1, 7, 37),
            datetime(2020, 1, 1, 7, 38)], tz=pytz.utc))

    df_output = di.detect_sunrise_sunset(
        df_input, 5.5755752, 50.6454981)

    df_expected = pd.DataFrame(
        data={'test': [0, 0, 0, 0, 5, 6]},
        index=df_input.index)

    pd.testing.assert_frame_equal(
        df_output, df_expected, check_dtype=False)


def test_detect_sunrise_sunset_end():
    """"""
    df_input = pd.DataFrame(
        data={'test': [3, 0, nan, nan, nan, nan]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 1, 15, 45),
            datetime(2020, 1, 1, 15, 46),
            datetime(2020, 1, 1, 15, 47),
            datetime(2020, 1, 1, 15, 48),
            datetime(2020, 1, 1, 15, 49),
            datetime(2020, 1, 1, 15, 50)], tz=pytz.utc))

    df_output = di.detect_sunrise_sunset(
        df_input, 5.5755752, 50.6454981)

    df_expected = pd.DataFrame(
        data={'test': [3, 0, 0, 0, 0, 0]},
        index=df_input.index)

    pd.testing.assert_frame_equal(
        df_output, df_expected, check_dtype=False)


def test_detect_sunrise_sunset_day():
    """Test that nan during the day is not replaced by 0"""
    df_input = pd.DataFrame(
        data={'test': [5, nan, nan, nan, 5, 7]},
        index=pd.DatetimeIndex([
            datetime(2020, 1, 1, 11, 45),
            datetime(2020, 1, 1, 11, 46),
            datetime(2020, 1, 1, 11, 47),
            datetime(2020, 1, 1, 11, 48),
            datetime(2020, 1, 1, 11, 49),
            datetime(2020, 1, 1, 11, 50)], tz=pytz.utc))

    df_output = di.detect_sunrise_sunset(
        df_input, 5.5755752, 50.6454981)

    pd.testing.assert_frame_equal(
        df_output, df_input, check_dtype=False)
