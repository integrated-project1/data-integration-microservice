"""
Integrated software project - Electrical energy at a glance - Team 2

Unit tests of the data_integration module

Author: Dominik Zians
"""

# Own modules
import data_update as du

import data_client.openapi_client as data_client
import auth_client.openapi_client as auth_client

# Standart library
import os
from datetime import datetime, timedelta
import logging

# Other
import pytest
import pandas as pd
import pytz


###############################################################################
# Test data                                                                   #
###############################################################################


# Production data

data_prod_init = pd.DataFrame(
    data={'power': [1000.0]},
    index=pd.DatetimeIndex([datetime(2020, 1, 1, 12, 0)], tz=pytz.utc))

data_prod_update = pd.DataFrame(
    data={'power': [1000.0, 1002.3, 999.8]},
    index=pd.DatetimeIndex(
        [
            datetime(2020, 1, 1, 12, 3),
            datetime(2020, 1, 1, 12, 4),
            datetime(2020, 1, 1, 12, 5)], tz=pytz.utc))

data_prod_expected = pd.DataFrame(
    data={'power': [1000.0, None, None, 1000.0, 1002.3, 999.8]},
    index=pd.DatetimeIndex([
        datetime(2020, 1, 1, 12, 0),
        datetime(2020, 1, 1, 12, 1),
        datetime(2020, 1, 1, 12, 2),
        datetime(2020, 1, 1, 12, 3),
        datetime(2020, 1, 1, 12, 4),
        datetime(2020, 1, 1, 12, 5)], tz=pytz.utc))


# Consumption data

data_cons_update = pd.DataFrame(
    data={
        'voltage_12' : [15500., 15500., 15502., 15509.],
        'voltage_23' : [15500., 15500., 15505., 15504.],
        'voltage_31' : [15500., 15500., 15510., 15504.],
        'current_1' : [30., 30.3, 30.5, 31],
        'current_2' : [30., 30.2, 30.7, 29.9],
        'current_3' : [30., 30.1, 30.7, 30],
        'active_power' : [400., 405., 401., 404.],
        'reactive_power' : [400., 410., 402., 408.],
        'cos_phi' : [0.99, 0.99, 0.98, 0.975]},
    index=pd.DatetimeIndex([
        datetime(2020, 1, 1, 0, 0),
        datetime(2020, 1, 1, 0, 3),
        datetime(2020, 1, 1, 0, 4),
        datetime(2020, 1, 1, 0, 5)], tz=pytz.utc))

data_cons_expected = pd.DataFrame(
    data={
        'voltage_12' : [15500., None, None, 15500., 15502., 15509.],
        'voltage_23' : [15500., None, None, 15500., 15505., 15504.],
        'voltage_31' : [15500., None, None, 15500., 15510., 15504.],
        'current_1' : [30., None, None, 30.3, 30.5, 31],
        'current_2' : [30., None, None, 30.2, 30.7, 29.9],
        'current_3' : [30., None, None, 30.1, 30.7, 30],
        'active_power' : [400., None, None, 405., 401., 404.],
        'reactive_power' : [400., None, None, 410., 402., 408.],
        'cos_phi' : [0.99, None, None, 0.99, 0.98, 0.975]},
    index=pd.DatetimeIndex([
        datetime(2020, 1, 1, 0, 0),
        datetime(2020, 1, 1, 0, 1),
        datetime(2020, 1, 1, 0, 2),
        datetime(2020, 1, 1, 0, 3),
        datetime(2020, 1, 1, 0, 4),
        datetime(2020, 1, 1, 0, 5)], tz=pytz.utc))


###############################################################################
# Fixtures                                                                    #
###############################################################################


@pytest.fixture(scope="module")  # Only execute once for the whole module
def data_configuration():
    # Configure auth-api
    auth_configuration = auth_client.Configuration(
        host=f"http://{os.environ['AUTH_API_HOST']}"
             f":{os.environ['AUTH_API_PORT']}/1.0.0")

    # Login to get a token
    with auth_client.ApiClient(auth_configuration) as api_client:
        # Create an instance of the API class
        auth_api_instance = auth_client.DefaultApi(api_client)

        # Create info for login
        authentication_info = auth_client.AuthenticationInfo(
            os.environ['API_USER'],
            os.environ['API_PASSWD'],
            response_type='token')

        # Get the access token
        login = auth_api_instance.login(authentication_info)
        api_token = login[2]["Location"].split('#')[1].split('&')[0].split('=')[1]

    # Configure data-api with access token
    data_configuration = data_client.Configuration(
        host=f"http://{os.environ['DATA_API_HOST']}"
             f":{os.environ['DATA_API_PORT']}/1.0.0")
    data_configuration.access_token = api_token

    return data_configuration


@pytest.fixture(scope="module")  # Only execute once for the whole module
def create_test_buildings(data_configuration):
    """A fixture that adds two test building

    In order to test the initialization capability of update_data, one building
    will already have data, the other not (only a date in last added)
    """
    with data_client.ApiClient(data_configuration) as api_client:
        # Create an instance of the API class
        api_instance = data_client.DefaultApi(api_client)

        # Add two test buildings
        test_building_1 = data_client.BuildingInfo(
            name='B1',
            type='production',
            location=[0., 0.],
            display_name='B1',
            map_datatype='power',
            datatypes=['power'],
            maxima=[100000],
            minima=[0])

        test_building_2 = data_client.BuildingInfo(
            name='B2',
            type='consumption',
            location=[0., 0.],
            display_name='B1',
            map_datatype='active_power',
            datatypes=[
                'voltage_12', 'voltage_23', 'voltage_31',
                'current_1', 'current_2', 'current_3',
                'active_power', 'reactive_power', 'cos_phi'],
            maxima=[16000, 16000, 16000, 50, 50, 50, 600, 600, 1],
            minima=[15000, 15000, 15000, 1, 1, 1, 1, 1, 0.5],
            last_added=datetime(2020, 1, 1, 0, 0, tzinfo=pytz.utc))

        api_instance.add_building(test_building_1)
        api_instance.post_data(
            'B1', data_prod_init.to_dict(orient='split'))

        api_instance.add_building(test_building_2)

    return (test_building_1, test_building_2)


###############################################################################
# ApiHandler                                                                  #
###############################################################################


def test_create_api_handler():
    """Test if the ApiHandler is created without exception"""
    # update_token() is tested here too (inside __init__())
    du.ApiHandler()


def test_get_buildings(create_test_buildings):
    api_handler = du.ApiHandler()

    # Expected output
    test_building_1, test_building_2 = create_test_buildings

    # Create dummy handlers
    handlers = {
        'B1': lambda s, e: None,
        'B2': lambda s, e: None}

    # Get information about all the buildings
    buildings = api_handler.get_buildings(handlers)

    # Check if all the buildings are there
    assert len(buildings) == 2

    # Check if the output is as expected
    for building in buildings:
        assert building.name in {'B1', 'B2'}
        if building.name == 'B1':
            expected = test_building_1
            # depending on the test order last_added differs, therefore
            # need to select the corresponding row from data_prod_expected
            data_last_added = data_prod_expected[
                data_prod_expected.index == building.last_added.index[0]]
            pd.testing.assert_frame_equal(
                building.last_added, data_last_added, check_like=True)
        else:
            expected = test_building_2
            # depending on the test order last_added differs, therefore
            # need to search the correct row from data_cons_expected
            # For this building need even to consider that last_added
            # is not a DataFrame but a datetime
            if isinstance(building.last_added, pd.DataFrame):
                data_last_added = data_cons_expected[
                    data_cons_expected.index == building.last_added.index[0]]
                pd.testing.assert_frame_equal(
                    building.last_added, data_last_added, check_like=True)
        assert building.name == expected.name
        assert building.typ == expected.type
        assert building.min_val == {
            dt: mi for dt, mi in zip(expected.datatypes, expected.minima)}
        assert building.max_val == {
            dt: ma for dt, ma in zip(expected.datatypes, expected.maxima)}


def test_missing_handler(create_test_buildings):
    api_handler = du.ApiHandler()
    # Create dummy handlers
    handlers = {
        'B1': lambda s, e: None}

    # Get information about all the buildings
    buildings = api_handler.get_buildings(handlers)  # should not raise an error

    # Check if all the other buildings are there
    assert len(buildings) == 1


###############################################################################
# Building                                                                    #
###############################################################################


def test_update_building(create_test_buildings, data_configuration):
    # Check if data is added by making get data afterwards
    api_handler = du.ApiHandler()

    # Create handlers, that return test data
    handlers = {
        'B1': lambda s, e: data_prod_update,
        'B2': lambda s, e: data_cons_update}

    # Get information about all the buildings
    buildings = api_handler.get_buildings(handlers)

    # Update the buildings
    for building in buildings:
        building.update_data(api_handler)

    # Check if the output is as expected
    with data_client.ApiClient(data_configuration) as api_client:
        # Create an instance of the API class
        api_instance = data_client.DefaultApi(api_client)

        data1 = du.api_data_to_dataframe(api_instance.get_data(
            'B1',
            datetime(2020, 1, 1, 12, 0, tzinfo=pytz.utc),
            datetime(2020, 1, 1, 12, 5, tzinfo=pytz.utc)))

        data2 = du.api_data_to_dataframe(api_instance.get_data(
            'B2',
            datetime(2020, 1, 1, 0, 0, tzinfo=pytz.utc),
            datetime(2020, 1, 1, 0, 5, tzinfo=pytz.utc)))

    logging.debug(f'{data1}\n{data_prod_expected}\n{data2}\n{data_cons_expected}')

    pd.testing.assert_frame_equal(data1, data_prod_expected, check_like=True)
    pd.testing.assert_frame_equal(data2, data_cons_expected, check_like=True)
